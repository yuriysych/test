package com.example.ysych.demigostestyuriysych;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by ysych on 06.01.2016.
 */
public class TabFragment extends Fragment {

    List<InfoEntry> infoEntries;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.tab_fragment, container, false);

        RecyclerView rv = (RecyclerView) view.findViewById(R.id.recycler_view);
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());


        rv.setLayoutManager(llm);

        try {
            infoEntries = HelperFactory.getHelper().getInfoEntryDAO().queryForAll();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        RecyclerViewAdapter adapter = new RecyclerViewAdapter(infoEntries, getContext());
        rv.setAdapter(adapter);

        return view;
    }
}
