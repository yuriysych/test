package com.example.ysych.demigostestyuriysych;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;

/**
 * Created by ysych on 06.01.2016.
 */
public class DatabaseHelper extends OrmLiteSqliteOpenHelper {

    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "demigostest.db";

    private InfoEntryDAO infoEntryDAO = null;

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase database, ConnectionSource connectionSource) {
        try
        {
            TableUtils.createTable(connectionSource, InfoEntry.class);
        }
        catch (SQLException e){
            throw new RuntimeException(e);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, ConnectionSource connectionSource, int oldVersion, int newVersion) {
        try{
            TableUtils.dropTable(connectionSource, InfoEntry.class, true);
            onCreate(database, connectionSource);
        }
        catch (SQLException e){
            throw new RuntimeException(e);
        }
    }

    public InfoEntryDAO getInfoEntryDAO() throws SQLException{
        if(infoEntryDAO == null){
            infoEntryDAO = new InfoEntryDAO(getConnectionSource(), InfoEntry.class);
        }
        return infoEntryDAO;
    }

    @Override
    public void close(){
        super.close();
        infoEntryDAO = null;
    }
}