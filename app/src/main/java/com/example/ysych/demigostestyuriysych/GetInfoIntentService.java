package com.example.ysych.demigostestyuriysych;

import android.app.IntentService;
import android.content.Intent;
import android.widget.Toast;

import java.sql.SQLException;
import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by ysych on 06.01.2016.
 */
public class GetInfoIntentService extends IntentService {

    private static String SERVICE_NAME = "GetInfoIntentService";
    private static String ENDPOINT_URL = "http://demigos.com";

    ARIRetrofit ariRetrofit;

    public GetInfoIntentService() {
        super(SERVICE_NAME);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(ENDPOINT_URL)
                .build();

        ariRetrofit = restAdapter.create(ARIRetrofit.class);

        ariRetrofit.getInfo(new Callback<Info>() {
            @Override
            public void success(Info info, Response response) {
                try {
                    List<InfoEntry> oldItems = HelperFactory.getHelper().getInfoEntryDAO().queryForAll();
                    HelperFactory.getHelper().getInfoEntryDAO().delete(oldItems);
                    List<InfoEntry> infoEntries = info.getData();
                    for (InfoEntry infoEntry : infoEntries){
                        HelperFactory.getHelper().getInfoEntryDAO().create(infoEntry);
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(getApplicationContext(), R.string.cant_get_info_from_server, Toast.LENGTH_SHORT).show();
            }
        });
    }
}
