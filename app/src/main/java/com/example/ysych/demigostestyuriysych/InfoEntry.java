package com.example.ysych.demigostestyuriysych;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by ysych on 06.01.2016.
 */
@DatabaseTable(tableName = "infoentry")
public class InfoEntry {

    @DatabaseField(canBeNull = false, dataType = DataType.STRING, columnName = "title")
    @SerializedName("title")
    @Expose
    private String title;

    @DatabaseField(canBeNull = false, dataType = DataType.STRING, columnName = "date")
    @SerializedName("date")
    @Expose
    private String date;

    @DatabaseField(canBeNull = false, dataType = DataType.STRING, columnName = "author")
    @SerializedName("author")
    @Expose
    private String author;

    @DatabaseField(canBeNull = false, dataType = DataType.STRING, columnName = "description")
    @SerializedName("description")
    @Expose
    private String description;

    @DatabaseField(canBeNull = false, dataType = DataType.STRING, columnName = "image")
    @SerializedName("image")
    @Expose
    private String image;

    public String getTitle() {
        return title;
    }

    public String getDate() {
        return date;
    }

    public String getAuthor() {
        return author;
    }

    public String getDescription() {
        return description;
    }

    public String getImage() {
        return image;
    }
}
