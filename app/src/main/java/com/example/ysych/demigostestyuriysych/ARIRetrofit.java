package com.example.ysych.demigostestyuriysych;

import retrofit.Callback;
import retrofit.http.GET;

/**
 * Created by ysych on 06.01.2016.
 */
public interface ARIRetrofit {

    static String SYNC_URL = "/test/releases.html";

    @GET(SYNC_URL)
    void getInfo(Callback<Info> callback);
}
