package com.example.ysych.demigostestyuriysych;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ysych on 06.01.2016.
 */
public class Info {

        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("data")
        @Expose
        private List<InfoEntry> data = new ArrayList<>();

        public List<InfoEntry> getData() {
                return data;
        }

        public String getStatus() {
                return status;
        }
}