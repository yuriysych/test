package com.example.ysych.demigostestyuriysych;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

/**
 * Created by ysych on 06.01.2016.
 */
public class InfoFragmentPagerAdapter extends FragmentPagerAdapter {

    public InfoFragmentPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        return new TabFragment();
    }

    @Override
    public int getCount() {
        return 4;
    }
}
