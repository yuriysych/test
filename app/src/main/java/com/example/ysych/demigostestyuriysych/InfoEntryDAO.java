package com.example.ysych.demigostestyuriysych;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;
import java.sql.SQLException;

/**
 * Created by ysych on 06.01.2016.
 */
public class InfoEntryDAO extends BaseDaoImpl<InfoEntry, Integer> {

    public InfoEntryDAO(ConnectionSource connectionSource,
                   Class<InfoEntry> dataClass) throws SQLException {
        super(connectionSource, dataClass);
    }
}