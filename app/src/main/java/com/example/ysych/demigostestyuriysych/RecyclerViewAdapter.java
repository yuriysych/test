package com.example.ysych.demigostestyuriysych;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.List;

/**
 * Created by ysych on 06.01.2016.
 */
public class RecyclerViewAdapter  extends RecyclerView.Adapter<RecyclerViewAdapter.TourViewHolder>{

    List<InfoEntry> infoEntries;
    Context context;

    public RecyclerViewAdapter(List<InfoEntry> tours, Context context) {
        this.infoEntries = tours;
        this.context = context;
    }

    @Override
    public TourViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_item_layout, parent, false);
        return new TourViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final TourViewHolder holder, final int position) {
        holder.infoTitle.setText(infoEntries.get(position).getTitle());
        holder.infoAuthor.setText(infoEntries.get(position).getAuthor());
        holder.infoDate.setText(infoEntries.get(position).getDate());
        holder.infoDescription.setText(infoEntries.get(position).getDescription());
        Glide.with(context)
                .load(infoEntries.get(position).getImage())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(holder.infoPhoto);
    }

    @Override
    public int getItemCount() {
        return infoEntries.size();
    }

    public static class TourViewHolder extends RecyclerView.ViewHolder {
        TextView infoTitle;
        TextView infoAuthor;
        TextView infoDate;
        TextView infoDescription;
        ImageView infoPhoto;

        TourViewHolder(View itemView) {
            super(itemView);
            infoTitle = (TextView)itemView.findViewById(R.id.item_layout_title);
            infoAuthor = (TextView)itemView.findViewById(R.id.item_layout_author);
            infoDate = (TextView)itemView.findViewById(R.id.item_layout_date);
            infoDescription = (TextView)itemView.findViewById(R.id.item_layout_description);
            infoPhoto = (ImageView)itemView.findViewById(R.id.item_layout_background_image);
        }
    }
}